import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class MakeGroupTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void makeGroups() {
        MakeGroup maker = new MakeGroup();
        maker.makeGroups(3, List.of("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"));
        assertTrue(maker.groupSize>0);
        assertTrue(maker.groupArray[0][0]!=null);
        System.out.println(maker.groupArray[2][2]);
    }
}