import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

public class WriteFileTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void arrayToFile() {
        WriteFile writer  = new WriteFile();
        Path pathName = Paths.get("C:\\Users\\nicol\\Documents\\Workspace\\group-ariel-nico\\groupDone.txt");
        String[][] arrayGroup ={{"AAA", "BBB", "CCC",},{"DDD", "EEE", "FFF"},{"GGG", "HHH", "III",},{"JJJ","KKK"}};
        writer.arrayToFile(pathName,arrayGroup);
    }
}