import java.nio.file.Paths;

public class Main {
    public static void main (String[] args){
        ReadNameFile reader = new ReadNameFile();
        MakeGroup maker = new MakeGroup();
        WriteFile writer = new WriteFile();
        reader.readToList();
        maker.makeGroups(7,reader.nameList);
        writer.arrayToFile(Paths.get("groupDone.txt"),maker.groupArray);
    }
}
