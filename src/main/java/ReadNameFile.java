import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class ReadNameFile {
    static final Path PERSON_LIST_PATH= Paths.get("PersonList.txt");


    List<String> nameList;
    int nbPerson;


    public ReadNameFile() {
        nbPerson=0;
        nameList = null;
    }

    public void readToList () {
        try {
            nameList = Files.readAllLines(PERSON_LIST_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.shuffle(nameList);
        nbPerson=nameList.size();

    }
}
