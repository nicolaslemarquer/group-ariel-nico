import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.APPEND;

public class WriteFile{
    Path pathName = Paths.get("groupDone.txt");

    public void arrayToFile(Path pathName, String[][] arrayGroup){
        try {
            for (int i=0; i<arrayGroup.length;i++){
                String groupNumber = "Groupe : " + (i+1) +"\n";
                Files.write(pathName, groupNumber.getBytes(), APPEND);
                for (int j=0;j<arrayGroup[i].length;j++){
                    Files.write(pathName, (arrayGroup[i][j] + "\n").getBytes(), APPEND);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
