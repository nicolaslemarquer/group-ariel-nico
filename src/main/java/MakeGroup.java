import java.util.List;

public class MakeGroup {
    int groupSize;
    String[][] groupArray;

    public void makeGroups(int nbGroup, List<String> nameList) {

        groupArray = new String[nbGroup][];
        groupSize = (nameList.size() / nbGroup) + 1;

        for (int i = 0; i < nbGroup; i++) {
            String[] groupUnit = new String[groupSize];
            for (int j = 0; j < groupSize; j++) {
                if ((i*groupSize + j)<nameList.size()) {
                    groupUnit[j] = nameList.get(i*groupSize + j);
                }
            }
            groupArray[i] = groupUnit;
        }
    }
}
